#lang racket
(require srfi/1)
(require srfi/26)
(require "../xexprs/src/htmlgen.rkt")
(require "../xexprs/src/xexprs.rkt")
(require "../xexprs/src/predicates.rkt")
(require "../xexprs/src/maybe.rkt")

(define abilities
  '((strength "Str" . "Strength")
    (dexterity "Dex" . "Dexterity")
    (constitution "Con" . "Constitution")
    (intelligence "Int" . "Intelligence")
    (wisdom "Wis" . "Wisdom")
    (charisma "Cha" . "Charisma")))

(define skills
  '((autohypnosis "Autohypnosis" . wisdom)
    (appraise "Appraise" . intelligence)
    (balance "Balance" . dexterity)
    (bluff "Bluff" . charisma)
    (climb "Climb" . strength)
    (concentration "Concentration" . constitution)
    (decipher-script "Decipher Script" . intelligence)
    (diplomacy "Diplomacy" . charisma)
    (disguise "Disguise" . charisma)
    (escape-artist "Escape Artist" . dexterity)
    (forgery "Forgery" . intelligence)
    (gather-information "Gather Information" . charisma)
    (handle-animal "Handle Animal" . charisma)
    (heal "Heal" . wisdom)
    (hide "Hide" . dexterity)
    (intimidate "Intimidate" . charisma)
    (jump "Jump" . strength)
    (knowledge-survival "Knowledge (Survival" . intelligence)
    (knowledge-social "Knowledge (Social)" . intelligence)
    (knowledge-magical "Knowledge (Magical)" . magical)
    (listen "Listen" . wisdom)
    (move-silently "Move Silently" . dexterity)
    (open-lock "Open Lock" . dexterity)
    (ride "Ride" . dexterity)
    (scent "Scent" . wisdom)
    (search "Search" . intelligence)
    (sense-motive "Sense Motive" . wisdom)
    (sleight-of-hand "Sleight of Hand" . dexterity)
    (spellcraft "Spellcraft" . intelligence)
    (spot "Spot" . wisdom)
    (survival "Survival" . wisdom)
    (swim "Swim" . strength)
    (tumble "Tumble" . dexterity)
    (use-magic-device "Use Magic Device" . charisma)
    (use-psionic-device "Use Psionic Device" . charisma)
    (use-rope "Use Rope" . dexterity)))

(define skill-synergies
  '((autohypnosis knowledge-magical)
    (bluff diplomacy (disguise . "to act in character") intimidate sleight-of-hand)
    (concentration autohypnosis)
    (craft appraise)
    (decipher-script (use-magic-device . "involving scrolls"))
    (escape-artist (use-rope . "involving bindings"))
    (handle-animal ride #| wild-empathy |#)
    (jump tumble)
    (knowledge-magical psicraft spellcraft (survival . "on other planes") #| turning-checks-against-undead |#)
    (knowledge-social #| bardic knowledge |# diplomacy gather-information (search . "involving secret doors and hidden compartments"))
    (knowledge-survival survival)
    (psicraft (use-psionic-device . "involving power stones"))
    (search (survival . "when following tracks"))
    (tumble balance jump)
    (use-magic-evice (spellcraft . "to decipher scrolls"))
    (use-psionic-device (psicraft . "to address power stones"))
    (use-rope (climb . "involving ropes") (escape-artist . "involving ropes"))))

(define lawfulnesses
  '((lawful . "Lawful")
    (neutral . "Neutral")
    (chaotic . "Chaotic")))

(define morality
  '((good . "Good")
    (neutral . "Neutral")
    (chaotic . "Chaotic")))

(define qualities
  '((scent . "Scent")
    (partial-night-vision . "Partial Night Vision")))

(define races
  '((megabat-hybrid
     (qualities scent partial-night-vision)
     (abilities
      (constitution . +1))
     (skills
      (listen . +1)))))

(define classes
  '((cultist
     (abilities
      (constitution . -1)
      (intelligence . +1)
      (wisdom . +1)))))

(define character-name
  '((name
     (firstname . "Character")
     (lastname . "Lastname"))
    (class . cultist)
    (level . 1)
    (race . megabat-hybrid)
    (alignment
     (lawfulness . lawful)
     (morality . good))
    (deity . sui)
    (age . 16)
    (gender . male)
    (size . medium)
    (height . "6'0")
    (weight . "170 lb")
    (abilities
     (strength .     16)
     (dexterity .    16)
     (constitution . 14)
     (intelligence . 15)
     (wisdom .       16)
     (charisma .     11))
    (skills
     (search . 5)
     (bluff . 4)
     (handle-animal . 4)
     (hide . 4)
     (jump . 4)
     (knowledge-social . 4)
     (knowledge-survival . 4)
     (knowledge-magical . 4)
     (listen . 2)
     (move-silently . 4)
     (scent . 4)
     (spot . 2)
     (tumble . 4))))

(define-contract (table/c-dep p x)
  (and ((listof (cons/c symbol? any/c)) x)
       (andmap (cute apply p <>) x)))

(define (table/c p)
  (table/c-dep (λ (x) (p (cdr x)))))

(define table? (listof (cons/c symbol? any/c)))

(define/contract (access-attribute table attribute)
  (-> table? symbol? any/c)
  (if (null? table) nothing
      (if (eq? attribute (caar table))
          (just (cdar table))
          (access-attribute (cdr table) attribute))))

(define-syntax access
  (syntax-rules ()
    [(access table) (just table)]
    [(access table attribute attributes ...)
     (>>= (access-attribute table (quasiquote attribute))
          (λ (subtable) (access subtable attributes ...)))]))

(define-syntax-rule (access-or-default default table attributes ...)
  (just-or-else default (access table attributes ...)))

(define-syntax-rule (access! table attributes ...)
  (car (access table attributes ...)))

(define/contract (get-ability-score-and-desc character ability)
  (-> table? symbol? (cons/c natural-number/c string?))
  (define base-score (access-or-default 10 character abilities ,ability))
  (define class-bonus (access-or-default 0 classes ,(access! character class) abilities ,ability))
  (define racial-bonus (access-or-default 0 races ,(access! character race) abilities ,ability))
  (cons (+ base-score class-bonus racial-bonus)
        (string-append (number->string base-score) " base, "
                       (number->string class-bonus) " class bonus, "
                       (number->string racial-bonus) " racial bonus")))

(define/contract (get-ability-score character ability)
  (-> table? symbol? natural-number/c)
  (car (get-ability-score-and-desc character ability)))

(define (calculate-ability-modifier ability)
  (quotient (- ability 10) 2))

(define (get-ability-modifier character ability)
  (calculate-ability-modifier (get-ability-score character ability)))

(define (get-skill-rank character skill)
  (access-or-default 0 character skills ,skill))

(define (list-contains? x list)
  (not (null? (filter (cute eq? x <>) list))))

(define (match-first pred list)
  (if (null? list) nothing
      (if (pred (car list))
          (just (car list))
          (match-first pred (cdr list)))))

(define (get-synergy-name synergy)
  (if (pair? synergy) (car synergy) synergy))

(define (get-all-skill-synergies character)
  (define rank-5-skills
    (map car
         (filter (λ (skill) (>= (cdr skill) 5))
                 (access! character skills))))
  (filter (λ (skill) (list-contains? (car skill) rank-5-skills)) skill-synergies))

(define (get-skill-synergies character skill)
  (filter-just
   (map (λ (synergy)
          (define synergy-skill-name (car synergy))
          (define synergy-skill-synergies (cdr synergy))
          (define matches-skill? (compose (cute eq? skill <>) get-synergy-name))
          (define skill-synergy (match-first matches-skill? synergy-skill-synergies))
          (map (λ (synergy-reason) (cons synergy-skill-name (cdr synergy-reason))) skill-synergy))
        (get-all-skill-synergies character))))

(define (get-applicable-skill-synergies character skill applicable)
  (filter (λ (synergy)
            (or (not (pair? synergy))
                (list-contains? (car synergy) applicable)))
          (get-skill-synergies character skill)))

(define (get-skill-synergy character skill applicable)
  (* 2 (length (get-applicable-skill-synergies character skill applicable))))

(define (get-skill-bonus character skill applicable)
  (define skill-rank (get-skill-rank character skill))
  (define skill-key-ability (cdr (access! skills ,skill)))
  (define class-bonus (access-or-default 0 classes ,(access! character class) skills ,skill))
  (define racial-bonus (access-or-default 0 races ,(access! character race) skills ,skill))
  (define ability-modifier (get-ability-modifier character skill-key-ability))
  (define skill-synergies (get-skill-synergy character skill applicable))
  (+ skill-rank class-bonus racial-bonus ability-modifier skill-synergies))

(define (get-total-ranks character)
  (apply + (map cdr (access! character skills))))

(display-to-file #:exists 'replace
 (html
 `(head
   ,meta-charset
   (title "Character Name")

   (meta #:name "copyright" #:content "James Martin 2019, CC BY-SA 4.0")
   (meta #:name "author"    #:content "James Martin")
   (link #:rel "canonical" #:href "https://jamestmartin.me/character")

   (meta #:rel "theme-color" #:content "#ffffff")
   (link #:rel "stylesheet" #:href "/res/style.css")
   (link #:rel "stylesheet" #:href "/res/character.css")

   ,@favicons

   (script #:type "text/javascript" #:src "/res/character.js"))
 `(body
   #:class "rp-character"
   (header
    (h1 #:class "rp-character-name" "Character Name")
    (aside #:class "card"
           (figure
            (figcaption
             ,(sectioned-header 'h3 "basic-info" "Basic Information")
             (br)
             ,(sectioned-header 'h3 "ability-scores" "Ability Scores")))))))
 "character-sheet.xhtml")